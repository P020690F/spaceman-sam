--Screensize 640x360
function love.load()
  if arg[#arg] == "-debug" then require("mobdebug").start() end
  
  math.randomseed(os.time())
  
  GameState = {Start = 1, Settings = 2, Leaderboard = 3, Game = 4, GameOver = 5}
  currentState = GameState.Start
  
  --Background initialisation
	background = love.graphics.newImage("Assets/Backgrounds/bg.png")
  backgroundQuad = love.graphics.newQuad(1,1,1280/2,720/2,1280/2,720/2)
  
  --Title Screen initialisation
  title = love.graphics.newImage("Assets/Title.png")
  playButton = love.graphics.newImage("Assets/Buttons/PlayBtn.png")
  restartButton = love.graphics.newImage("Assets/Buttons/RestartBtn.png")
  gameoverButton = love.graphics.newImage("Assets/Buttons/GameoverBtn.png")
  homeButton = love.graphics.newImage("Assets/Buttons/HomeBtn.png")
  bigTitleQuad = love.graphics.newQuad(1,1,177,119,177,119)
  smallTitleQuad = love.graphics.newQuad(1,1,91,87,91,87)
  
  --Blackhole initialisation
  --bhPathPoints = {{x=0,y=0},{x=320,y=0},{x=640,y=0},{x=260,y=180},{x=640,y=360},{x=360,y=360},{x=0,y=360},{x=0,y=180}}
  bhPathPoints = {{x=-90,y=-90},{x=270,y=-90},{x=550,y=-90},{x=550,y=90},{x=550,y=270},{x=270,y=270},{x=-90,y=270},{x=-90,y=90}}
  bhIndex = math.random(1,8)
  blackhole = {
    Image = love.graphics.newImage("Assets/Backgrounds/Blackhole.png"),
    Quad = love.graphics.newQuad(1, 1, 180, 180, 180, 180),
    x = bhPathPoints[bhIndex].x,
    y = bhPathPoints[bhIndex].y,
    rotation = 0
  }
  
  --Powerup initialisation
  gunAngle = 0
  Powerup = {}
  table.insert(Powerup, {Image = love.graphics.newImage("Assets/Powerups/Shotgun.png"), Quad =  love.graphics.newQuad(1, 1, 30, 30, 30, 30), x = math.random(50,550), y = math.random(50,300), p = "shotgun"} )
  table.insert(Powerup, {Image = love.graphics.newImage("Assets/Powerups/Laser.png"), Quad =  love.graphics.newQuad(1, 1, 30, 30, 30, 30), x = math.random(50,550), y = math.random(50,300), p = "laser"} )
  pistol = { Image = love.graphics.newImage("Assets/Weapons/Default.png"), Quad =  love.graphics.newQuad(1, 1, 16, 13, 16, 13) }
  shotgun = { Image = love.graphics.newImage("Assets/Weapons/Shotgun.png"), Quad =  love.graphics.newQuad(1, 1, 52, 12, 52, 12) }
  laser = { Image = love.graphics.newImage("Assets/Weapons/Laser.png"), Quad =  love.graphics.newQuad(1, 1, 18, 10, 18, 10) }
  
  --Font initialistion
  scoreFont = love.graphics.newFont(12)
  font = love.graphics.newFont("Assets/Fonts/Calisto.ttf", 40)
  
  --Sound initialisation
  defaultGunSound = love.audio.newSource("Assets/Sounds/DefaultGun.mp3", "static")
  shotgunSound = love.audio.newSource("Assets/Sounds/Shotgun.mp3", "static")
  laserSound = love.audio.newSource("Assets/Sounds/Laser.mp3", "static")
  crumblingSound = love.audio.newSource("Assets/Sounds/Crumbling.mp3", "static")
  
  --Player character initialisation
  player = {
    Image = love.graphics.newImage("Assets/Player/Spaceman_Sam.png"),
    Quad = love.graphics.newQuad(1,1,30,64,30,64),
    x = 360,
    y = 180,
    alive = true,
    powerup = "none"
  }
  
  --Randomly decides starting direction of player
  if (math.random(1, 2)/2 == 1) then
    playerMovement_x = 1
  else
    playerMovement_x = -1
  end
  if (math.random(1, 2)/2 == 1) then
    playerMovement_y = 1
  else
    playerMovement_y = -1
  end
  
  --General asteroid initialisation
  startX = 0
  startY = 0
  asteroidRotate = 0
  
  --Small asteroid initialisation
  smallAsteroidList = {}
  sAsteroidImage = love.graphics.newImage("Assets/Obstacles/Small_asteroid.png")
  newSmallAsteroids(3)

  --Large asteroid initialisation
  largeAsteroidList = {}
  lAsteroidImage = love.graphics.newImage("Assets/Obstacles/Large_asteroid.png")
  newLargeAsteroids(1)

  --Bullet initialisation
  bulletSpeed = 250
	bullets = {}
  fireRate = 100
  
  --Score initialisation
  score = 0
end

function Reset()
  --Player character initialisation
  table.remove(player, 1)
  player = {
    Image = love.graphics.newImage("Assets/Player/Spaceman_Sam.png"),
    Quad = love.graphics.newQuad(1,1,30,64,30,64),
    x = 45,
    y = 200,
    alive = true,
    powerup = "none"
  }
    
  --Randomly decides starting direction of player
  if (math.random(1, 2)/2 == 1) then
    playerMovement_x = 1
  else
    playerMovement_x = -1
  end
  if (math.random(1, 2)/2 == 1) then
    playerMovement_y = 1
  else
    playerMovement_y = -1
  end
  
  --Blackhole initialisation
  bhIndex = math.random(1,8)
  blackhole.x = bhPathPoints[bhIndex].x
  blackhole.y = bhPathPoints[bhIndex].y
  
  --General asteroid initialisation
  startX = 0
  startY = 0
  
  --Deletes bullets
  for i = 1, table.maxn(bullets) do
    table.remove(bullets, 1)
  end
  
  --Small asteroid initialisation
  for i = 1, table.maxn(smallAsteroidList) do
    table.remove(smallAsteroidList, i)
  end
  smallAsteroidList = {}
  newSmallAsteroids(3)

  --Large asteroid initialisation
  for i = 1, table.maxn(largeAsteroidList) do
    table.remove(largeAsteroidList, i)
  end
  largeAsteroidList = {}
  newLargeAsteroids(1)
  
  --Score initialisation
  score = 0
  
  --Powerup initialisation 
  Powerup = {}
  table.insert(Powerup, { Image = love.graphics.newImage("Assets/Powerups/Shotgun.png"), Quad =  love.graphics.newQuad(1, 1, 30, 30, 30, 30), x = math.random(50,550), y = math.random(50,300), p = "shotgun"} )
  table.insert(Powerup, { Image = love.graphics.newImage("Assets/Powerups/Laser.png"), Quad =  love.graphics.newQuad(1, 1, 30, 30, 30, 30), x = math.random(50,550), y = math.random(50,300), p = "laser"} )
  
  currentState = GameState.Game
end

function love.draw()
  --Draws background
  love.graphics.draw(background, backgroundQuad, 0, 0)
  
  if (currentState ~= GameState.Game) then
    love.graphics.setFont(font)
  else
    love.graphics.setFont(scoreFont)
  end
  
  if (currentState == GameState.Game) then
    --Draws blackhole
    love.graphics.draw(blackhole.Image, blackhole.Quad, blackhole.x, blackhole.y)
    
    --Draws powerups
    for i = 1, table.maxn(Powerup) do
      love.graphics.draw(Powerup[i].Image, Powerup[i].Quad, Powerup[i].x, Powerup[i].y)
    end
    
    --Prints current score
    love.graphics.print("Score:", 0, 0, 0, 1, 1)
    love.graphics.print(score, 45, 0, 0, 1, 1)
    
    --Draws player
    love.graphics.draw(player.Image, player.Quad, player.x, player.y)--, asteroidRotate/2, 1, 1, 30/2, 64/2)
    if (player.powerup == "none") then
      love.graphics.draw(pistol.Image, pistol.Quad, player.x + 2, player.y + 32, gunAngle)
    elseif (player.powerup == "shotgun") then
      love.graphics.draw(shotgun.Image, shotgun.Quad, player.x + 2, player.y + 32, gunAngle)
    elseif (player.powerup == "laser") then
      love.graphics.draw(laser.Image, laser.Quad, player.x + 2, player.y + 32, gunAngle)
    end
    
    --Draws all small asteroids
    for i,sa in ipairs(smallAsteroidList) do
        love.graphics.draw(sa.Image, sa.Quad, sa.x, sa.y, asteroidRotate, 1, 1, 45/2, 45/2)
    end
    
    --Draws all large asteroids
    for i,la in ipairs(largeAsteroidList) do
        love.graphics.draw(la.Image, la.Quad, la.x, la.y, asteroidRotate, 1, 1, 115/2, 95/2)
    end
    
    --Draws bullets
    if player.powerup == "laser" then
      love.graphics.setColor(255, 0, 0)
    else
      love.graphics.setColor(128, 128, 128)
    end

    for i,b in ipairs(bullets) do
      love.graphics.circle("fill", b.x, b.y, 3)
    end
    
    love.graphics.setColor(255,255,255)
  elseif (currentState == GameState.GameOver) then
    love.graphics.setColor(255, 0, 0)
    love.graphics.draw(gameoverButton, bigTitleQuad, 230, 10, 0, 1, 1)
    love.graphics.setColor(255,255,255)
    love.graphics.draw(restartButton, smallTitleQuad, 300, 210, 0, 1, 1)
    love.graphics.draw(homeButton, smallTitleQuad, 10, 260, 0, 1, 1)
  elseif (currentState == GameState.Start) then
    love.graphics.draw(title, bigTitleQuad, 230, 10, 0, 1, 1)
    love.graphics.draw(playButton, smallTitleQuad, 100, 160, 0, 1, 1)
  end
end

function love.update(dt)
  --Adds score to player so long as they are alive
  if (currentState == GameState.Game) then
    score = score + 1
    
  --Limits firerate of guns
    fireRate = fireRate + 1
    if (fireRate > 49) then
      shotgunSound:stop()
      defaultGunSound:stop()
      laserSound:stop()
    end
    
  --Moves bullets
    for i,b in ipairs(bullets) do
      b.x = b.x + (b.dx * dt)
      b.y = b.y + (b.dy * dt)
    end
      
    --Moves small asteroids
    for i,sa in ipairs(smallAsteroidList) do
      sa.x = sa.x + (sa.dx * dt * 0.5)
      sa.y = sa.y + (sa.dy * dt * 0.5)
    end
      
    --Moves large asteroids
    for i,la in ipairs(largeAsteroidList) do
      la.x = la.x + (la.dx * dt * 0.3)
      la.y = la.y + (la.dy * dt * 0.3)
    end
      
    --Checks collision between bullets and large asteroids, if there is a collision, destroys the asteroid and bullet that collided with it and increases the player's score
    for i,la in ipairs(largeAsteroidList) do
      for j,b in ipairs(bullets) do
        if(CheckCollision(la.x - 57, la.y - 47, 115, 95, b.x, b.y, 3, 3)) then
          crumblingSound:play()
          table.remove(largeAsteroidList, i)
          table.remove(bullets, j)
          newLargeAsteroids(1)
          score = score + 100
        end
      end
    end
      
    --Checks collision between bullets and small asteroids, if there is a collision, destroys the asteroid and bullet that collided with it and increases the player's score
    for i,sa in ipairs(smallAsteroidList) do
      for j,b in ipairs(bullets) do
        if(CheckCollision(sa.x, sa.y, 45, 45, b.x, b.y, 3, 3)) then
          crumblingSound:play()
          table.remove(smallAsteroidList, i)
          table.remove(bullets, j)
          newSmallAsteroids(1)
          score = score + 25
        end
      end
    end
      
    --Checks collision between bullets and powerups, if there is a collision, destroys the powerup and bullet that collided with it and changes the player's current weapon
    for i,pu in ipairs(Powerup) do
      for j,b in ipairs(bullets) do
        if(CheckCollision(pu.x, pu.y, 30, 30, b.x, b.y, 3, 3)) then
          table.remove(Powerup, i)
          table.remove(bullets, j)
          player.powerup = pu.p
        end
      end
    end
      
    --Kills the player character if they collide with a large asteroid
    for i,la in ipairs(largeAsteroidList) do
      if(CheckCollision(la.x, la.y, 115, 95, player.x, player.y, 30, 64)) then
        player.alive = false
      end
    end
      
    --Kills the player character if they collide with a small asteroid
    for i,sa in ipairs(smallAsteroidList) do
      if(CheckCollision(sa.x, sa.y, 45, 45, player.x, player.y, 30, 64)) then
        player.alive = false
      end
    end
    --Changes asteroid rotation value so they turn
    asteroidRotate = asteroidRotate + 0.01
    if (asteroidRotate == 1) then
      asteroidRotate = 0
    end
    
    --Moves the player character
    player.x = player.x + playerMovement_x
    player.y = player.y + playerMovement_y
    
    --Calls Blackhole update
    UpdateBlackhole(dt)
    
    --Calls viewport collision
    ViewportCollisons()
    
    if (player.alive == false) then
      currentState = GameState.GameOver
      
      
    end
  end
end

function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  --Checks collision between two objects
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end

function ViewportCollisons()
  --Checks if player character is at the edge of the screen
  if (player.x == 0) then
    playerMovement_x = 1
  end
  if (player.y == 0) then
    playerMovement_y = 1
  end
  if (player.x + 30 == 1280/2) then
    playerMovement_x = -1
  end
  if (player.y + 64 == 720/2) then
    playerMovement_y = -1
  end
  
  --Destroys bullets as they leave the screen
  for i,b in ipairs(bullets) do
    if (b.x < 1280/2 and b.x > 0) and (b.y < 720/2 and b.y > 0) then
      check = false
    else
      table.remove(bullets, i)
    end
  end
  
  for i,sa in ipairs(smallAsteroidList) do
    if (sa.x < 1280/2 and sa.x > 0) and (sa.y < 720/2 and sa.y > 0) then
      sa.check = true
    end
    if (sa.check) then
      if (sa.x < 1280/2 and sa.x > 0) and (sa.y < 720/2 and sa.y > 0) then
        sa.check = true
      else
        table.remove(smallAsteroidList, i)
        if (table.maxn(smallAsteroidList) < 6) then
          newSmallAsteroids(2)
        else
          newSmallAsteroids(1)
        end
      end
    end  
  end
  
  for i,la in ipairs(largeAsteroidList) do
    if (la.x < 1280/2 and la.x > 0) and (la.y < 720/2 and la.y > 0) then
      la.check = true
    end
    if (la.check) then
      if (la.x < 1280/2 and la.x > 0) and (la.y < 720/2 and la.y > 0) then
        la.check = true
      else
        table.remove(largeAsteroidList, i)
        if (table.maxn(largeAsteroidList) < 3) then
          newLargeAsteroids(2)
        else
          newLargeAsteroids(1)
        end
      end
    end  
  end
end

function love.mousepressed(x, y, button)
	if button == 1 then
    --Bullet starts at center of player and aims towards the current mouse position
		local startX = player.x + 15
		local startY = player.y + 32
		local mouseX = x
		local mouseY = y
		local angle = math.atan2((mouseY - startY), (mouseX - startX))
    gunAngle = angle
		local bulletDx = bulletSpeed * math.cos(angle)
    local bulletDy = bulletSpeed * math.sin(angle)
    
    --Spawns a new bullet, if in game.
    if (currentState == GameState.Game) then
      if (fireRate > 50) then
        if (player.powerup == "shotgun") then
          shotgunSound:play()
          table.insert(bullets, {x = startX, y = startY, dx = bulletDx, dy = bulletDy})
          table.insert(bullets, {x = startX, y = startY, dx = (bulletSpeed * math.cos(angle - 0.2)), dy = (bulletSpeed * math.sin(angle - 0.2))})
          table.insert(bullets, {x = startX, y = startY, dx = (bulletSpeed * math.cos(angle + 0.2)), dy = (bulletSpeed * math.sin(angle + 0.2))})
          fireRate = 0
        elseif (player.powerup == "laser") then
          for i = 1, 25 do
            laserSound:play()
            table.insert(bullets, {x = startX, y = startY, dx = ((bulletSpeed - i) * math.cos(angle)), dy = ((bulletSpeed - i) * math.sin(angle))})
            fireRate = 0
          end
        else
          defaultGunSound:play()
          table.insert(bullets, {x = startX, y = startY, dx = bulletDx, dy = bulletDy})
          fireRate = 30
        end
      end
    elseif (currentState == GameState.GameOver) then
      if(CheckCollision(x, y, 3, 3, 300, 210, 91, 87)) then
        Reset()
      elseif (CheckCollision(x, y, 3, 3, 10, 260, 91, 87)) then
        currentState = GameState.Start 
      end
    elseif (currentState == GameState.Start) then
      if(CheckCollision(x, y, 3, 3, 100, 160, 91, 87)) then
        currentState = GameState.Game
      end
    end
  end
end

function randomStartXY()
  if (bhIndex == 1) then
    startX = blackhole.x + (550 - math.random(50, 400))
    startY = blackhole.y + 550
  elseif (bhIndex == 2) then
    local r = math.random(1,2)
    if (r == 1) then
      startX = blackhole.x + math.random(50, 200)
    else
      startX = blackhole.x - math.random(50, 200)
    end
    startY = blackhole.y + 550
  elseif (bhIndex == 3) then
    startX =  blackhole.x - (550 - math.random(50, 400))
    startY = blackhole.y + 550
  elseif (bhIndex == 4) then
    local r = math.random(1,2)
    if (r == 1) then 
      startY = blackhole.y + 550
    else
      startY = blackhole.y - 550
    end
    startX = blackhole.x - math.random(200, 500)
  elseif (bhIndex == 5) then
    startX = blackhole.x - (550 - math.random(50, 400))
    startY = blackhole.y - 300
  elseif (bhIndex == 6) then
    local r = math.random(1,2)
    if (r == 1) then
      startX = blackhole.x + math.random(50, 200)
    else
      startX = blackhole.x - math.random(50, 200)
    end
    startY = blackhole.y - 550
  elseif (bhIndex == 7) then
    startX =  blackhole.x + (550 - math.random(50, 400))
    startY = blackhole.y - 550 
  elseif (bhIndex == 8) then
    local r = math.random(1,2)
    if (r == 1) then 
      startY = blackhole.y + 550
    else
      startY = blackhole.y - 550
    end
    startX = blackhole.x + math.random(200, 500)
  end
end

function newSmallAsteroids(i)
  for k=1, i do
    randomStartXY()
    
    table.insert(smallAsteroidList, {
        Image = sAsteroidImage, 
        Quad = love.graphics.newQuad(1,1,45,45,45,45),
        x = startX,
        y = startY,
        dx = blackhole.x+90 - startX,
        dy = blackhole.y+90 - startY,
        check = false
        })
  end
end

function newLargeAsteroids(i) 
    for k=1, i do
    randomStartXY()
    
    table.insert(largeAsteroidList, {
        Image = lAsteroidImage, 
        Quad = love.graphics.newQuad(1,1,115,95,115,95), 
        x = startX,
        y = startY,
        dx = blackhole.x - startX,
        dy = blackhole.y - startY,
        check = false
        })
  end
end

function UpdateBlackhole(dt)
  local dx
  local dy
  local dirx
  local diry
  
  if (bhIndex == 8)then
    dx = bhPathPoints[1].x - bhPathPoints[bhIndex].x
    dy = bhPathPoints[1].y - bhPathPoints[bhIndex].y
    dirx = bhPathPoints[1].x - blackhole.x
    diry = bhPathPoints[1].y - blackhole.y
  else
    dx = bhPathPoints[bhIndex+1].x - bhPathPoints[bhIndex].x
    dy = bhPathPoints[bhIndex+1].y - bhPathPoints[bhIndex].y
    dirx = bhPathPoints[bhIndex+1].x - blackhole.x
    diry = bhPathPoints[bhIndex+1].y - blackhole.y
  end
  
  if (math.sqrt(dirx*dirx + diry*diry) < 1) then
    if (bhIndex == 8)then
      bhIndex = 1
    else
      bhIndex = bhIndex + 1
    end
  else
    blackhole.x = blackhole.x + (dx * dt* 0.025)
    blackhole.y = blackhole.y + (dy * dt* 0.025)
  end
end